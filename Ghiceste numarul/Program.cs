﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ghiceste_numarul
{
    class Program
    {
        static void Main(string[] args)
        {
            Random Rand = new Random();
            int numarRandom = Rand.Next(100);

            int numarUtilizator;
            bool ghicit = false;
            int incercari = 0;
            while (ghicit == false)
            {
                Console.WriteLine("Ghiciti numarul");

                while (int.TryParse(Console.ReadLine(), out numarUtilizator) == false || numarUtilizator > 100)
                {
                    Console.WriteLine("Nu ai introdus un numar valid, indroduce alt numar:");
                }

                incercari++;


                if(numarUtilizator==numarRandom)
                {
                    Console.WriteLine($"Felicitari!Ai ghicit numarul din {incercari} incercari" );
                    ghicit = true;
                }
                else
                {
                    int diferentaAbs = Math.Abs(numarRandom - numarUtilizator);

                    if(diferentaAbs<=3)
                    {
                        Console.WriteLine("Foarte fierbinte");
                    }
                    else if(diferentaAbs<=5)
                    {
                        Console.WriteLine("Fierbinte");
                    }
                    else if(diferentaAbs<=10)
                    {
                        Console.WriteLine("Cald");
                    }
                    else if(diferentaAbs<=20)
                    {
                        Console.WriteLine("Caldut");
                    }
                    else if(diferentaAbs<=50)
                    {
                        Console.WriteLine("Rece");
                    }
                    else if(diferentaAbs>50)
                    {
                        Console.WriteLine("Foarte rece");
                    }

                }
                Console.WriteLine();
                Console.WriteLine();
            }



            Console.ReadKey();
        }
        
    }
}
